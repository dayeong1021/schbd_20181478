import csv

# 총액을 저장할 임시 변수
with open('sample3.csv', mode='r',encoding='utf-8') as f:
	reader = csv.reader(f)
	total_cost =0
	for i, row in enumerate(reader):
		if i ==0: continue
		# 현재 읽어들인 과일에 대한 액수 계산
		row_sum = int(row[1]) * int(row[2])
		print("{} cost = {}".format(row[0],row_sum))
		#계산한 액수를 총액에 누적
		total_cost += row_sum
		#총액을 화면에 출력
	print("-----------------------------")
	print("Total cost = {}".format(total_cost))











